const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    isCustomer: Boolean,
    email: String,
    date: {type: Date, default: Date.now}
})

const User = mongoose.model('user', userSchema) // MongoDb renombra las colecciones agregando una 's' al final del nombre, en este caso 'user' paso a ser 'users'

module.exports = User