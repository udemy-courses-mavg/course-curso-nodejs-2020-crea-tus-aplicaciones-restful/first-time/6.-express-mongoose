const express = require('express')
const Sale = require('../models/sale')
const Car = require('../models/car')
const User = require('../models/user')
const router = express.Router()
const mongoose = require('mongoose')

/** ============================================================================= 
 * ==================================== GET =====================================
 * ============================================================================== */

router.get('/', async(req, res)=> {
    const sales = await Sale.find()
    res.send(sales)
})

/*router.get('/:id', async(req, res)=> {
    const company = await Company.findById(req.params.id)
    if(!company) 
        return res.status(404).send('No hemos encontrado un fabricante con ese ID')
    res.send(company)
})*/

/** ============================================================================= 
 * ==================================== POST ====================================
 * ============================================================================== */

router.post('/', async(req, res)=>{
    const user = await User.findById(req.body.userId)
    if(!user) return res.status(400).send('El usuasrio no existe')
    
    const car = await Car.findById(req.body.carId)
    if(!car) return res.status(400).send('El carro no existe')

    if(car.sold === true) return res.status(400).send('Ese coche ya ha sido vendido')

    const sale = new Sale({
        user:{
            _id: user._id, // Se puede utilizar así debido a que se trajo el usuario desde la declaración de la constante "user"
            name: user.name,
            email: user.email
        },
        car: {
            _id: car._id,
            model: car.model
        },
        price: req.body.price
    })

    /*const result = await sale.save()
    // Antes de las siguientes dos líneas se puede validar si el usuario ya es cliente para omitir esas líneas
    user.isCustomer = true
    user.save()
    // Antes de las siguientes dos líneas también se puede validar si el carro ya fue comprado para omitir errores en la base de datos
    car.sold = true
    car.save()
    res.status(201).send(result)*/

    // ============================= TRANSACTIONS ============================= 
    const session = await mongoose.startSession()
    session.startTransaction()
    try {
        const result = await sale.save()
        user.isCustomer = true
        user.save()
        car.sold = true
        car.save()
        await session.commitTransaction()
        session.endSession()
        res.status(201).send(result)
    } catch(e) {
        await session.abortTransaction()
        session.endSession()
        res.status(500).send(e.message)
    }

})

module.exports = router