const express = require('express')
const router = express.Router()
const Car = require('../models/car')
const {Company} = require('../models/company')
const { check, validationResult } = require('express-validator');

/** ============================================================================= 
 * ==================================== GET =====================================
 * ============================================================================== */

router.get('/', async(req, res)=> {
    //const cars = await Car.find()
    const cars = await Car
        .find()
        //.populate('company', 'name country') // Accede a la coleccion especificada, Se indican los cambios que se quieren enviar
    res.send(cars)
})

router.get('/:id', async(req, res)=> {
    const car = await Car.findById(req.params.id)
    if(!car) 
        return res.status(404).send('No hemos encontrado un conche con ese ID')
    res.send(car)
})

/** ============================================================================= 
 * ==================================== POST ====================================
 * ============================================================================== */

 // POST Modelo de datos Embebido
router.post('/', [
    check('year').isLength({min: 3}),
    check('model').isLength({min: 3})
], async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty())
        return res.status(422).json({ errors: errors.array() });

    const company = await Company.findById(req.body.companyId)
    if(!company) 
        return res.status(400).send('No tenemos ese fabricante')

    const car = new Car({
        company: company,
        model: req.body.model,
        sold: req.body.sold,
        price: req.body.price,
        year: req.body.year,
        extras: req.body.extras        
    })

    const result = await car.save()
    res.status(201).send(result)
})

 // POST Modelo de datos Normalizado
 /*
router.post('/', [
    check('model').isLength({min: 3})
], async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty())
        return res.status(422).json({ errors: errors.array() });

    const car = new Car({
        company: req.body.company,
        model: req.body.model,
        sold: req.body.sold,
        price: req.body.price,
        year: req.body.year,
        extras: req.body.extras        
    })

    const result = await car.save()
    res.status(201).send(result)
})
*/

/** ============================================================================= 
 * ==================================== PUT =====================================
 * ============================================================================== */

router.put('/:id', [
    check('company').isLength({min: 4}),
    check('model').isLength({min: 3})
], async(req, res)=>{
    const errors = validationResult(req);
    
    if (!errors.isEmpty())
        return res.status(422).json({ errors: errors.array() });

    const car = await Car.findByIdAndUpdate(req.params.id, {
        company: req.body.company,
        model: req.body.model,
        sold: req.body.sold,
        price: req.body.price,
        year: req.body.year,
        extras: req.body.extras 
    },
    {
        new: true
    })

    if(!car)
        return res.status(404).send('El coche con ese ID no esta')
    res.status(204).send(car)

})

/** ============================================================================= 
 * ================================== DELETE ====================================
 * ============================================================================== */

router.delete('/:id', async(req, res)=>{
    const car = await Car.findByIdAndDelete(req.params.id)
    if(!car)
        return res.status(404).send('El coche con ese ID no esta, no se puede borrar')
    res.status(200).send('Coche borrado')
})

module.exports = router